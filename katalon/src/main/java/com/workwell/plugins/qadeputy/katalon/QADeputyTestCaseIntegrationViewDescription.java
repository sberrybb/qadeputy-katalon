package com.workwell.plugins.qadeputy.katalon;

import com.katalon.platform.api.exception.ResourceException;
import com.katalon.platform.api.extension.TestCaseIntegrationViewDescription;
import com.katalon.platform.api.extension.TestSuiteIntegrationViewDescription;
import com.katalon.platform.api.model.ProjectEntity;
import com.katalon.platform.api.preference.PluginPreference;

public class QADeputyTestCaseIntegrationViewDescription implements TestCaseIntegrationViewDescription
{
    @Override
    public boolean isEnabled(ProjectEntity projectEntity) {
        PluginPreference pluginStore = null;
        try {
            pluginStore = CommonUtils.getPluginStore();
        } catch (ResourceException e) {
            return false;
        }

        boolean returnValue=pluginStore.getBoolean(Constants.PREF_ENABLED, false);
        return returnValue;
    }

    @Override
    public String getName() {
        return "QADeputy";
    }

    @Override
    public Class<? extends TestCaseIntegrationView > getTestCaseIntegrationView() {
        return QADeputyTestCaseIntegrationView.class;
    }
}
