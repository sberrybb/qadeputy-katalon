package com.workwell.plugins.qadeputy.katalon;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TestRuns {
    @SerializedName("test_run_id")
    @Expose
    private Integer testRunId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("test_suite_id")
    @Expose
    private Integer testSuiteId;
    @SerializedName("test_suite_name")
    @Expose
    private String testSuiteName;
    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("test_run_status")
    @Expose
    private String testRunStatus;
    @SerializedName("test_run_mode")
    @Expose
    private String testRunMode;
    @SerializedName("total_test_cases_count")
    @Expose
    private Integer totalTestCasesCount;
    @SerializedName("completed_test_cases_count")
    @Expose
    private Integer completedTestCasesCount;
    @SerializedName("passed_test_cases_count")
    @Expose
    private Integer passedTestCasesCount;
    @SerializedName("failed_test_cases_count")
    @Expose
    private Integer failedTestCasesCount;
    @SerializedName("not_testing_cases_count")
    @Expose
    private Integer notTestingCasesCount;
    @SerializedName("time_remaining")
    @Expose
    private String timeRemaining;
    @SerializedName("reviewed_users")
    @Expose
    private List<Object> reviewedUsers = null;
    @SerializedName("completed_at")
    @Expose
    private Object completedAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getTestRunId() {
        return testRunId;
    }

    public void setTestRunId(Integer testRunId) {
        this.testRunId = testRunId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public Integer getTestSuiteId() {
        return testSuiteId;
    }

    public void setTestSuiteId(Integer testSuiteId) {
        this.testSuiteId = testSuiteId;
    }

    public String getTestSuiteName() {
        return testSuiteName;
    }

    public void setTestSuiteName(String testSuiteName) {
        this.testSuiteName = testSuiteName;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getTestRunStatus() {
        return testRunStatus;
    }

    public void setTestRunStatus(String testRunStatus) {
        this.testRunStatus = testRunStatus;
    }

    public String getTestRunMode() {
        return testRunMode;
    }

    public void setTestRunMode(String testRunMode) {
        this.testRunMode = testRunMode;
    }

    public Integer getTotalTestCasesCount() {
        return totalTestCasesCount;
    }

    public void setTotalTestCasesCount(Integer totalTestCasesCount) {
        this.totalTestCasesCount = totalTestCasesCount;
    }

    public Integer getCompletedTestCasesCount() {
        return completedTestCasesCount;
    }

    public void setCompletedTestCasesCount(Integer completedTestCasesCount) {
        this.completedTestCasesCount = completedTestCasesCount;
    }

    public Integer getPassedTestCasesCount() {
        return passedTestCasesCount;
    }

    public void setPassedTestCasesCount(Integer passedTestCasesCount) {
        this.passedTestCasesCount = passedTestCasesCount;
    }

    public Integer getFailedTestCasesCount() {
        return failedTestCasesCount;
    }

    public void setFailedTestCasesCount(Integer failedTestCasesCount) {
        this.failedTestCasesCount = failedTestCasesCount;
    }

    public Integer getNotTestingCasesCount() {
        return notTestingCasesCount;
    }

    public void setNotTestingCasesCount(Integer notTestingCasesCount) {
        this.notTestingCasesCount = notTestingCasesCount;
    }

    public String getTimeRemaining() {
        return timeRemaining;
    }

    public void setTimeRemaining(String timeRemaining) {
        this.timeRemaining = timeRemaining;
    }

    public List<Object> getReviewedUsers() {
        return reviewedUsers;
    }

    public void setReviewedUsers(List<Object> reviewedUsers) {
        this.reviewedUsers = reviewedUsers;
    }

    public Object getCompletedAt() {
        return completedAt;
    }

    public void setCompletedAt(Object completedAt) {
        this.completedAt = completedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
