package com.workwell.plugins.qadeputy.katalon;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TestRunCreateRequest {


    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("test_suite")
    @Expose
    private Integer testSuite;
    @SerializedName("users")
    @Expose
    private List<Integer> users = null;
    @SerializedName("include_all")
    @Expose
    private Boolean includeAll;
    @SerializedName("test_cases")
    @Expose
    private List<Integer> testCases = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getTestSuite() {
        return testSuite;
    }

    public void setTestSuite(Integer testSuite) {
        this.testSuite = testSuite;
    }

    public List<Integer> getUsers() {
        return users;
    }

    public void setUsers(List<Integer> users) {
        this.users = users;
    }

    public Boolean getIncludeAll() {
        return includeAll;
    }

    public void setIncludeAll(Boolean includeAll) {
        this.includeAll = includeAll;
    }

    public List<Integer> getTestCases() {
        return testCases;
    }

    public void setTestCases(List<Integer> testCases) {
        this.testCases = testCases;
    }
}
