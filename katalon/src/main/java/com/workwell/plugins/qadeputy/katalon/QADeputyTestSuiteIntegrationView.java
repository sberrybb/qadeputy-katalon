package com.workwell.plugins.qadeputy.katalon;

import com.katalon.platform.api.exception.ResourceException;
import com.katalon.platform.api.extension.TestSuiteIntegrationViewDescription;
import com.katalon.platform.api.model.Integration;
import com.katalon.platform.api.model.TestSuiteEntity;
import com.katalon.platform.api.preference.PluginPreference;
import com.katalon.platform.api.service.ApplicationManager;
import com.katalon.platform.api.service.ControllerManager;
import com.katalon.platform.api.ui.UISynchronizeService;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

import java.util.ArrayList;
import java.util.List;

public class QADeputyTestSuiteIntegrationView implements TestSuiteIntegrationViewDescription.TestSuiteIntegrationView {
    private Shell parentShell;
    private Composite container;
    private Group grpAuthentication;
    private Combo txtSuite;
    private Thread thread;
    private List<TestRuns> testRuns;
    private List<TestCase> testCases;
    private List<TestSuites> testSuites;
    private Label lblConnectionStatus;
    private TestSuiteEntity testSuiteEntity;
    private QaDebutyTestSuiteIntegration integrationItem;
    private boolean edit;

    private TestSuiteIntegrationViewDescription.PartActionService partActionService;

    @Override
    public Control onCreateView(Composite composite, TestSuiteIntegrationViewDescription.PartActionService partActionService, TestSuiteEntity testSuiteEntity) {
        parentShell=composite.getShell();
        container = new Composite(composite, SWT.NONE);
        integrationItem= (QaDebutyTestSuiteIntegration) testSuiteEntity.getIntegration(Constants.TestSUITEIntegrationID);

        container.setLayout(new GridLayout(1, false));
        this.partActionService=partActionService;
        this.testSuiteEntity =testSuiteEntity;

        grpAuthentication = new Group(container, SWT.NONE);
        grpAuthentication.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
        GridLayout glAuthentication = new GridLayout(2, false);
        glAuthentication.horizontalSpacing = 15;
        glAuthentication.verticalSpacing = 10;
        grpAuthentication.setLayout(glAuthentication);
        grpAuthentication.setText("TestRun Mapping");
        List<String> testSuites=new ArrayList<>();


        for(int x=0;x<testSuites.size();x++) {
            final int indexNumber=x;
            createLabel("Mapped TestRun # " + x + ":");
            Combo testRun = createComboBox();
            fillTestRun(testRun, testSuites.get(x));


            final Button btnAddNew = new Button(grpAuthentication, SWT.PUSH);
            btnAddNew.setText("New TestRun");
            btnAddNew.addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent e) {
                    AddNewTestRun( testSuites.get(indexNumber));
                }
            });
        }
        return container;
    }

    private void fillTestRun(Combo testRun, String suiteName) {
        final Shell parentShell = new Shell(testRun.getDisplay());

        parentShell.setText("Loading TestSuites...");
        parentShell.setSize(350, 100);
        ProgressBar progressBar = new ProgressBar(parentShell, SWT.INDETERMINATE);
        progressBar.setBounds(40, 40, 200, 20);
        parentShell.open();
        Rectangle parentSize = testRun.getDisplay().getBounds();
        Rectangle shellSize = parentShell.getBounds();
        int locationX = (parentSize.width - shellSize.width) / 2 + parentSize.x;
        int locationY = (parentSize.height - shellSize.height) / 2 + parentSize.y;
        parentShell.setLocation(new Point(locationX, locationY));
        parentShell.layout();

        Thread thread = new Thread(() -> {

            final  List<TestRuns>   testRuns = WebService.getInstance().getTestRuns(suiteName);
            if (testRuns.size() != 0) {
                syncExec(() -> {
                    for (TestRuns runItem : testRuns) {
                        testRun.add(runItem.getName());

                    }

                    testRun.addModifyListener(new ModifyListener() {
                        @Override
                        public void modifyText(ModifyEvent modifyEvent) {

                        }
                    });
                    parentShell.close();
                });
            } else {
                syncExec(() -> {
                    parentShell.close();
                    MessageDialog.openWarning(parentShell, "Warning", "There is an issue with the provided credentials or no TestSuites are configured");
                });
            }


        });
        thread.start();
    }

    private void AddNewTestRun(   String suiteName) {



    }







    void syncExec(Runnable runnable) {
        ApplicationManager.getInstance()
                .getUIServiceManager()
                .getService(UISynchronizeService.class)
                .syncExec(runnable);

    }
    private void createLabel(String labelText) {
        Label label = new Label(grpAuthentication, SWT.NONE);
        label.setText(labelText);
        GridData gridData = new GridData(SWT.LEFT, SWT.TOP, false, false);
        label.setLayoutData(gridData);
    }

    private Combo createComboBox() {
        Combo text = new Combo(grpAuthentication, SWT.BORDER);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        gridData.widthHint = 200;

        text.setLayoutData(gridData);
        return text;
    }
    private Text createTextBox() {
        Text text = new Text(grpAuthentication, SWT.BORDER);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        gridData.widthHint = 200;

        text.setLayoutData(gridData);
        return text;
    }

    @Override
    public Integration getIntegrationBeforeSaving() {
        edit=false;
        return integrationItem;
    }

    @Override
    public boolean needsSaving() {
        return edit;
    }
}
