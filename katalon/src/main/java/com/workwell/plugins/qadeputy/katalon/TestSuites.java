package com.workwell.plugins.qadeputy.katalon;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TestSuites {
    @SerializedName("test_suite_id")
    @Expose
    private Integer testSuiteId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("product")
    @Expose
    private String product;
    @SerializedName("test_features_count")
    @Expose
    private Integer testFeaturesCount;
    @SerializedName("test_cases_count")
    @Expose
    private Integer testCasesCount;

    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getTestSuiteId() {
        return testSuiteId;
    }

    public void setTestSuiteId(Integer testSuiteId) {
        this.testSuiteId = testSuiteId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Integer getTestFeaturesCount() {
        return testFeaturesCount;
    }

    public void setTestFeaturesCount(Integer testFeaturesCount) {
        this.testFeaturesCount = testFeaturesCount;
    }

    public Integer getTestCasesCount() {
        return testCasesCount;
    }

    public void setTestCasesCount(Integer testCasesCount) {
        this.testCasesCount = testCasesCount;
    }


    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
