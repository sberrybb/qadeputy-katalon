package com.workwell.plugins.qadeputy.katalon;

import com.katalon.platform.api.controller.TestCaseController;
import com.katalon.platform.api.model.Integration;
import com.katalon.platform.api.model.TestCaseEntity;

import java.util.HashMap;
import java.util.Map;

public class QaDebutyTestCaseIntegration implements Integration {
    private String testCaseId;
    private String testSuiteId;
    private String suiteName;
    private String testCaseName;
    private String lastStartedTestRun;
    private TestCaseController controller;
    private TestCaseEntity entity;


    @Override
    public String getName() {
        return  Constants.TestCaseIntegrationID;
    }

    @Override
    public Map<String, String> getProperties() {
        HashMap<String, String> props = new HashMap<>();
        props.put( Constants.PREF_TESTSUITEID, getTestSuiteId());
        props.put( Constants.PREF_TESTCASEID, getTestCaseId());
        props.put( Constants.PREF_TESTSUITENAME, getSuiteName());
        props.put( Constants.PREF_TESTCASENAME, getTestCaseName());
        props.put( Constants.PREF_LASTTESTRUN, getLastStartedTestRun());
        return props;
    }

    public void setTestCaseId(String testCaseId) {
        this.testCaseId = testCaseId;
    }

    public String getTestCaseId() {
        return testCaseId;
    }

    public String getTestSuiteId() {
        return testSuiteId;
    }

    public void setTestSuiteId(String testSuiteId) {
        this.testSuiteId = testSuiteId;
    }

    public String getSuiteName() {
        return suiteName;
    }

    public void setSuiteName(String suiteName) {
        this.suiteName = suiteName;
    }

    public void setTestCaseName(String testCaseName) {
        this.testCaseName = testCaseName;
    }

    public String getTestCaseName() {
        return testCaseName;
    }

    public void setProperties(Map<String, String> props) {
        testSuiteId=props.get( Constants.PREF_TESTSUITEID);
        testCaseId=props.get( Constants.PREF_TESTCASEID);
        suiteName=props.get( Constants.PREF_TESTSUITENAME);
        testCaseName=props.get( Constants.PREF_TESTCASENAME);
        lastStartedTestRun=props.get(Constants.PREF_LASTTESTRUN);
    }

    public void setLastStartedTestRun(String lastStartedTestRun) {
        this.lastStartedTestRun = lastStartedTestRun;
    }

    public String getLastStartedTestRun() {
        return lastStartedTestRun;
    }

    public void setController(TestCaseController entity) {
        this.controller = entity;
    }

    public TestCaseController getController() {
        return controller;
    }

    public void setEntity(TestCaseEntity entity) {
        this.entity = entity;
    }

    public TestCaseEntity getEntity() {
        return entity;
    }
}
