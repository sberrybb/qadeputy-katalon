package com.workwell.plugins.qadeputy.katalon;

import com.katalon.platform.api.exception.ResourceException;
import com.katalon.platform.api.extension.TestSuiteIntegrationViewDescription;
import com.katalon.platform.api.model.Integration;
import com.katalon.platform.api.model.ProjectEntity;
import com.katalon.platform.api.model.TestSuiteEntity;
import com.katalon.platform.api.preference.PluginPreference;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

public class QADeputyTestSuiteIntegrationViewDescription implements TestSuiteIntegrationViewDescription
{
    @Override
    public boolean isEnabled(ProjectEntity projectEntity) {
        PluginPreference pluginStore = null;
        try {
            pluginStore = CommonUtils.getPluginStore();
        } catch (ResourceException e) {
            return false;
        }

        boolean returnValue=pluginStore.getBoolean(Constants.PREF_ENABLED, false);
        return returnValue;
    }

    @Override
    public String getName() {
        return "QaDeputy";
    }

    @Override
    public Class<? extends TestSuiteIntegrationView> getTestSuiteIntegrationView() {
        return QADeputyTestSuiteIntegrationView.class;
    }
}
