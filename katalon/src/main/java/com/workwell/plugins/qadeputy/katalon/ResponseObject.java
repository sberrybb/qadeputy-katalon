package com.workwell.plugins.qadeputy.katalon;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseObject {
    @SerializedName("data")
    @Expose
    private List<TestCase> testCaseList;
    @SerializedName("meta")
    @Expose
    private MetaData metaData;


    public List<TestCase> getTestCaseList() {
        return testCaseList;
    }

    public void setTestCaseList(List<TestCase> testCaseList) {
        this.testCaseList = testCaseList;
    }

    public MetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }
}
