package com.workwell.plugins.qadeputy.katalon;

import com.katalon.platform.api.exception.ResourceException;
import com.katalon.platform.api.extension.DynamicQueryingTestSuiteDescription;
import com.katalon.platform.api.extension.TestCaseIntegrationViewDescription;
import com.katalon.platform.api.model.Integration;
import com.katalon.platform.api.model.ProjectEntity;
import com.katalon.platform.api.model.TestCaseEntity;
import com.katalon.platform.api.model.TestSuiteEntity;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import java.util.ArrayList;
import java.util.List;

public class QADeputyTestSuiteQueryView implements DynamicQueryingTestSuiteDescription {

    @Override
    public String getQueryingType() {
        return "QaDeputy";
    }

    @Override
    public List<TestCaseEntity> query(ProjectEntity projectEntity, TestSuiteEntity testSuiteEntity, String s) throws ResourceException {
        return new ArrayList<TestCaseEntity>();
    }
}
