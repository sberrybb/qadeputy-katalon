package com.workwell.plugins.qadeputy.katalon;

import com.katalon.platform.api.extension.ToolItemDescription;
import com.katalon.platform.api.service.ApplicationManager;
import com.katalon.platform.api.ui.DialogActionService;

public class QADeputyToolItemDescription implements ToolItemDescription {
    @Override
    public String toolItemId() {
        return Constants.PLUGIN_ID +".QADeputyToolItemDescription";
    }

    @Override
    public String name() {
        return "QADeputy";
    }

    @Override
    public String iconUrl() {
        return   "platform:/plugin/" + Constants.PLUGIN_ID + "/icons/icon.png";
    }

    @Override
    public void handleEvent() {
        ApplicationManager.getInstance().getUIServiceManager().getService(DialogActionService.class).openPluginPreferencePage(
                Constants.PREF_PAGE_ID);
    }
}
