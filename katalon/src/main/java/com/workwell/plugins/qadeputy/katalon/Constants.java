package com.workwell.plugins.qadeputy.katalon;

public  class Constants {

    public static final String PREF_PAGE_ID = "com.workwell.plugins.qadeputy.katalon.QaDeputyPluginPreferencePage";
    public static final String PLUGIN_ID ="com.workwell.plugins.qadeputy.katalon.qadeputy-katalon-plugin";
    public static final String PREF_ENABLED = "QA_Deputy_Enabled";
    public static final String PREF_USERNAME = "QA_Deputy_USERNAME";
    public static final String PREF_APIKEY = "QA_Deputy_APIKEY";
    public static final String PREF_PROJECTNAME ="QA_Deputy_PROJECTNAME" ;
    public static final String PREF_PROJECTID ="QA_Deputy_PROJECTID" ;
    public static final String PREF_TESTSUITENAME = "QA_Deputy_TESTSUITENAME";
    public static final String PREF_TESTSUITEID = "QA_Deputy_TESTSUITEID";
    public static final String PREF_TESTCASEID = "QA_Deputy_TESTCASEID";
    public static final String TestCaseIntegrationID="com.workwell.plugins.qadeputy.katalon.QaDebutyTestCaseIntegration";
    public static final String TestSUITEIntegrationID="com.workwell.plugins.qadeputy.katalon.QaDebutyTestCSuiteIntegration";
    public static final String PREF_TESTCASENAME ="QA_Deputy_TESTCASENAME" ;
    public static final String PREF_LASTTESTRUN = "QA_Deputy_TESTLASTTESTRUN";
}
