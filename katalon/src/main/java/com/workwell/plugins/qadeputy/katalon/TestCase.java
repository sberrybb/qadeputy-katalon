package com.workwell.plugins.qadeputy.katalon;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TestCase {

    @SerializedName("test_case_id")
    @Expose
    private Integer testCaseId;
    @SerializedName("test_feature_id")
    @Expose
    private Integer testFeatureId;
    @SerializedName("test_feature")
    @Expose
    private String testFeature;
    @SerializedName("test_suite")
    @Expose
    private String testSuite;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("preconditions")
    @Expose
    private String preconditions;
    @SerializedName("test_case_steps")
    @Expose
    private String testCaseSteps;
    @SerializedName("expected_results")
    @Expose
    private String expectedResults;
    @SerializedName("specifications")
    @Expose
    private String specifications;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("last_run_time")
    @Expose
    private String lastRunTime;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private String deletedAt;

    public Integer getTestCaseId() {
        return testCaseId;
    }

    public void setTestCaseId(Integer testCaseId) {
        this.testCaseId = testCaseId;
    }

    public Integer getTestFeatureId() {
        return testFeatureId;
    }

    public void setTestFeatureId(Integer testFeatureId) {
        this.testFeatureId = testFeatureId;
    }

    public String getTestFeature() {
        return testFeature;
    }

    public void setTestFeature(String testFeature) {
        this.testFeature = testFeature;
    }

    public String getTestSuite() {
        return testSuite;
    }

    public void setTestSuite(String testSuite) {
        this.testSuite = testSuite;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPreconditions() {
        return preconditions;
    }

    public void setPreconditions(String preconditions) {
        this.preconditions = preconditions;
    }

    public String getTestCaseSteps() {
        return testCaseSteps;
    }

    public void setTestCaseSteps(String testCaseSteps) {
        this.testCaseSteps = testCaseSteps;
    }

    public String getExpectedResults() {
        return expectedResults;
    }

    public void setExpectedResults(String expectedResults) {
        this.expectedResults = expectedResults;
    }

    public String getSpecifications() {
        return specifications;
    }

    public void setSpecifications(String specifications) {
        this.specifications = specifications;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLastRunTime() {
        return lastRunTime;
    }

    public void setLastRunTime(String lastRunTime) {
        this.lastRunTime = lastRunTime;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }
}
