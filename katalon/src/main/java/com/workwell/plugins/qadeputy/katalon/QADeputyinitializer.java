package com.workwell.plugins.qadeputy.katalon;

import com.katalon.platform.api.controller.TestCaseController;
import com.katalon.platform.api.event.EventListener;
import com.katalon.platform.api.event.ExecutionEvent;
import com.katalon.platform.api.exception.ResourceException;
import com.katalon.platform.api.execution.TestSuiteExecutionContext;
import com.katalon.platform.api.extension.EventListenerInitializer;
import com.katalon.platform.api.model.Integration;
import com.katalon.platform.api.model.ProjectEntity;
import com.katalon.platform.api.model.TestCaseEntity;
import com.katalon.platform.api.preference.PluginPreference;
import com.katalon.platform.api.service.ApplicationManager;
import com.katalon.platform.api.ui.UISynchronizeService;
import org.eclipse.swt.widgets.Display;
import org.osgi.service.event.Event;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QADeputyinitializer implements EventListenerInitializer {
Display dp;

    @Override
    public void registerListener(EventListener eventListener) {

        eventListener.on(Event.class, event -> {




                if (ExecutionEvent.TEST_SUITE_STARTED_EVENT.equals(event.getTopic())) {
                    try {PluginPreference preferences = CommonUtils.getPluginStore();

                                    boolean isIntegrationEnabled = preferences.getBoolean(Constants.PREF_ENABLED, false);
                                    if (!isIntegrationEnabled) {
                                        return;
                                    }


                                    ExecutionEvent eventObject = (ExecutionEvent) event.getProperty("org.eclipse.e4.data");
                                    TestSuiteExecutionContext testSuiteContext = (TestSuiteExecutionContext) eventObject
                                            .getExecutionContext();
                                    ProjectEntity project = ApplicationManager.getInstance().getProjectManager().getCurrentProject();
                                    TestCaseController controller = ApplicationManager.getInstance().getControllerManager().getController(TestCaseController.class);
                                    List<TestCaseEntity> testcases = new ArrayList<>();
                        List<TestCaseController> controlers = new ArrayList<>();
                                    testSuiteContext.getTestCaseContexts().stream().forEach(testCaseExecutionContext -> {
                                        try {
                                            testcases.add(controller.getTestCase(project, testCaseExecutionContext.getId()));
                                            controlers.add(controller);
                                        } catch (ResourceException e) {
                                            e.printStackTrace();
                                        }
                                    });
                                    List<String> testSuiteIds = new ArrayList<>();
                        List<String> testSuiteNames= new ArrayList<>();
                        Map<String,List<QaDebutyTestCaseIntegration>> testCaseIntegrations=new HashMap<>();
                                    for (TestCaseEntity testcase : testcases
                                    ) {
                                        Integration testCase = (Integration) testcase.getIntegration(Constants.TestCaseIntegrationID);
                                        QaDebutyTestCaseIntegration  integration=new QaDebutyTestCaseIntegration();
                                        integration.setProperties(testCase.getProperties());
                                        if(integration.getTestSuiteId()!=null&&integration.getTestCaseId()!=null&&!testSuiteIds.contains(integration.getTestSuiteId())) {
                                            testSuiteIds.add(integration.getTestSuiteId());
                                            testSuiteNames.add(integration.getSuiteName());

                                        }
                                        if(integration.getTestSuiteId()!=null&&integration.getTestCaseId()!=null&&!testCaseIntegrations.containsKey(integration.getTestSuiteId())) {
                                            testCaseIntegrations.put(integration.getTestSuiteId(), new ArrayList<>());
                                        }
                                        testCaseIntegrations.get(integration.getTestSuiteId()).add(integration);
                                        integration.setController(controlers.get(testcases.indexOf(testcase)));
                                        integration.setEntity(testcase);
                                    }

                        for(String id:testSuiteIds){

                        List<Integer> ids=new ArrayList<>();
                            testCaseIntegrations.get(id).forEach(c-> {
                                if(c.getTestCaseId()!=null&&c.getTestCaseId()!=null){
                                    ids.add(Integer.parseInt(c.getTestCaseId()));
                                }
                            });

                          String currentTestRunID=  WebService.getInstance().CreateTestRun(id,testSuiteNames.get(testSuiteIds.indexOf(id)),ids);
                          testCaseIntegrations.get(id).forEach(c->{
                              if(c.getTestCaseId()!=null&&c.getTestCaseId()!=null){

                              c.setLastStartedTestRun(currentTestRunID);
                              try {
                                  c.getController().updateIntegration(project,c.getEntity(),c);
                              } catch (ResourceException e) {
                                  e.printStackTrace();
                              }
                              }
                          });

                        }


                                } catch (Exception exp) {
                                    exp.printStackTrace();
                                }

                        }else if(ExecutionEvent.TEST_SUITE_FINISHED_EVENT.equals(event.getTopic()))
                {
                    try{
                    PluginPreference preferences = CommonUtils.getPluginStore();

                    boolean isIntegrationEnabled = preferences.getBoolean(Constants.PREF_ENABLED, false);
                    if (!isIntegrationEnabled) {
                        return;
                    }

                    ExecutionEvent eventObject = (ExecutionEvent) event.getProperty("org.eclipse.e4.data");
                    TestSuiteExecutionContext testSuiteContext = (TestSuiteExecutionContext) eventObject
                            .getExecutionContext();

                    ProjectEntity project = ApplicationManager.getInstance().getProjectManager().getCurrentProject();
                    TestCaseController controller = ApplicationManager.getInstance().getControllerManager().getController(TestCaseController.class);

                    List<Long> updateIds = new ArrayList<>();
                    testSuiteContext.getTestCaseContexts().stream().forEach(testCaseExecutionContext -> {
                        String status = testCaseExecutionContext.getTestCaseStatus();
                        status=mapStatus(status);
                        TestCaseEntity tc=null;
                        try {
                            tc=controller.getTestCase(project,testCaseExecutionContext.getId());
                        } catch (ResourceException e) {
                            e.printStackTrace();
                        }
                        if(tc!=null) {
                            Integration testCase = (Integration) tc.getIntegration(Constants.TestCaseIntegrationID);
QaDebutyTestCaseIntegration integration=new QaDebutyTestCaseIntegration();
                            integration.setProperties(testCase.getProperties());

                            WebService.getInstance().updateTestrun(Integer.parseInt(status), integration.getLastStartedTestRun(), integration.getTestCaseId());

                        }
                    });
                }catch (Exception exp){
                        exp.printStackTrace();
                    }
                }
                    });
                }

    private String mapStatus(String kstatus) {
        String status;
        switch (kstatus) {
            case "PASSED":
                status = "3"; //PASSED
                break;
            case "FAILED":
            case "ERROR":
                status = "4"; //FAILED
                break;
            default:
                status = "0";
        }
        return status;
    }


    void syncExec(Runnable runnable) {
             ApplicationManager.getInstance()
                    .getUIServiceManager()
                    .getService(UISynchronizeService.class)
                    .syncExec(runnable);

    }
    }
