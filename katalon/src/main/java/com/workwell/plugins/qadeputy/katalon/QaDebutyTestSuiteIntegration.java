package com.workwell.plugins.qadeputy.katalon;

import com.katalon.platform.api.model.Integration;

import java.util.HashMap;
import java.util.Map;

public class QaDebutyTestSuiteIntegration implements Integration {
    private String testSuiteId;
    private String suiteName;


    @Override
    public String getName() {
        return Constants.TestCaseIntegrationID;
    }

    @Override
    public Map<String, String> getProperties() {
        HashMap<String, String> props = new HashMap<>();
        props.put(Constants.PREF_TESTSUITEID, getTestSuiteId());

        props.put(Constants.PREF_TESTSUITENAME, getSuiteName());

        return props;
    }


    public String getTestSuiteId() {
        return testSuiteId;
    }

    public void setTestSuiteId(String testSuiteId) {
        this.testSuiteId = testSuiteId;
    }

    public String getSuiteName() {
        return suiteName;
    }

    public void setSuiteName(String suiteName) {
        this.suiteName = suiteName;
    }


}
