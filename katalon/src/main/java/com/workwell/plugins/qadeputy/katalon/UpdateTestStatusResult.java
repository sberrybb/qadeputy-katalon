package com.workwell.plugins.qadeputy.katalon;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UpdateTestStatusResult {

    @SerializedName("previous_value")
    @Expose
    private List<PreviousValue> previousValue = null;
    @SerializedName("updated_value")
    @Expose
    private List<UpdatedValue> updatedValue = null;

    public List<PreviousValue> getPreviousValue() {
        return previousValue;
    }

    public void setPreviousValue(List<PreviousValue> previousValue) {
        this.previousValue = previousValue;
    }

    public List<UpdatedValue> getUpdatedValue() {
        return updatedValue;
    }

    public void setUpdatedValue(List<UpdatedValue> updatedValue) {
        this.updatedValue = updatedValue;
    }

}
