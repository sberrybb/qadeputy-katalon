package com.workwell.plugins.qadeputy.katalon;

import com.katalon.platform.api.model.Integration;
import com.katalon.platform.api.model.TestCaseEntity;
import com.katalon.platform.api.service.ApplicationManager;
import com.katalon.platform.api.ui.UISynchronizeService;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

import java.util.ArrayList;
import java.util.List;

public class TestRunHelper {
    private  List<TestCaseEntity> testcases;
    private boolean running;
    private Composite container;
    private Group grpAuthentication;


    private Text newTestRun;

    public TestRunHelper(List<TestCaseEntity> testcases) {
        this.running = false;
        this.testcases=testcases;
    }


    public void execute(Display dp) {
        running=true;
        Thread t=new Thread(new Runnable() {


            @Override
            public void run() {
                try{
                    Display d2=new Display( );
                   final Shell s = new Shell(d2);
                    syncExec(new Runnable() {
                        @Override
                        public void run() {

                            s.open();
                        }
                    });

                }catch (Exception exp){
exp.printStackTrace();
                }finally {
running=false;
                }
            }
        });
t.start();
    }

    private void fillTestRun(Combo testRun, String suiteName) {
        final Shell parentShell = new Shell(testRun.getDisplay());

        parentShell.setText("Loading TestSuites...");
        parentShell.setSize(350, 100);
        ProgressBar progressBar = new ProgressBar(parentShell, SWT.INDETERMINATE);
        progressBar.setBounds(40, 40, 200, 20);
        parentShell.open();
        Rectangle parentSize = testRun.getDisplay().getBounds();
        Rectangle shellSize = parentShell.getBounds();
        int locationX = (parentSize.width - shellSize.width) / 2 + parentSize.x;
        int locationY = (parentSize.height - shellSize.height) / 2 + parentSize.y;
        parentShell.setLocation(new Point(locationX, locationY));
        parentShell.layout();

        Thread thread = new Thread(() -> {

          final  List<TestRuns>   testRuns = WebService.getInstance().getTestRuns(suiteName);
            if (testRuns.size() != 0) {
                syncExec(() -> {
                    for (TestRuns runItem : testRuns) {
                        testRun.add(runItem.getName());

                    }

                    testRun.addModifyListener(new ModifyListener() {
                        @Override
                        public void modifyText(ModifyEvent modifyEvent) {

                        }
                    });
                    parentShell.close();
                });
            } else {
                syncExec(() -> {
                    parentShell.close();
                    MessageDialog.openWarning(parentShell, "Warning", "There is an issue with the provided credentials or no TestSuites are configured");
                });
            }


        });
        thread.start();
    }





    void syncExec(Runnable runnable) {
                  ApplicationManager.getInstance()
                    .getUIServiceManager()
                    .getService(UISynchronizeService.class)
                    .syncExec(runnable);

    }
    private void createLabel(String labelText) {
        Label label = new Label(grpAuthentication, SWT.NONE);
        label.setText(labelText);
        GridData gridData = new GridData(SWT.LEFT, SWT.TOP, false, false);
        label.setLayoutData(gridData);
    }

    private Combo createComboBox() {
        Combo text = new Combo(grpAuthentication, SWT.BORDER);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        gridData.widthHint = 200;

        text.setLayoutData(gridData);
        return text;
    }
    private Text createTextBox() {
        Text text = new Text(grpAuthentication, SWT.BORDER);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        gridData.widthHint = 200;

        text.setLayoutData(gridData);
        return text;
    }
    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }
}
