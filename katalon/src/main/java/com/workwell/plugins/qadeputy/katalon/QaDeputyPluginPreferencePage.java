package com.workwell.plugins.qadeputy.katalon;

import com.katalon.platform.api.extension.PluginPreferencePage;
import org.eclipse.jface.preference.PreferencePage;

public class QaDeputyPluginPreferencePage implements PluginPreferencePage {
    @Override
    public String getName() {
        return "QADeputy";
    }

    @Override
    public String getPageId() {
        return "com.workwell.plugins.qadeputy.katalon.QaDeputyPluginPreferencePage";
    }

    @Override
    public Class<? extends PreferencePage> getPreferencePageClass() {
        return QADeputyPreferencePage.class;
    }
}
