package com.workwell.plugins.qadeputy.katalon;


import com.katalon.platform.api.extension.TestCaseIntegrationViewDescription;
import com.katalon.platform.api.model.Integration;
import com.katalon.platform.api.model.TestCaseEntity;
import com.katalon.platform.api.service.ApplicationManager;
import com.katalon.platform.api.ui.UISynchronizeService;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

import java.util.List;

public class QADeputyTestCaseIntegrationView implements TestCaseIntegrationViewDescription.TestCaseIntegrationView  {
    private Shell parentShell;
    private Composite container;
    private Group grpAuthentication;
    private Combo txtSuite;
    private Thread thread;
    private List<TestSuites> suitesList;
    private Label lblConnectionStatus;
    private TestCaseEntity testCaseEntity;
    private Combo txtCase;
    private boolean edit=false;
    private QaDebutyTestCaseIntegration integrationItem;
    private List<TestCase> caseList;
    private TestCaseIntegrationViewDescription.PartActionService partActionService;

    @Override
    public Control onCreateView(Composite composite, TestCaseIntegrationViewDescription.PartActionService partActionService, TestCaseEntity testCaseEntity) {
        parentShell=composite.getShell();
        this.partActionService=partActionService;
        container = new Composite(composite, SWT.NONE);
        container.setLayout(new GridLayout(1, false));
        this.testCaseEntity =testCaseEntity;

        grpAuthentication = new Group(container, SWT.NONE);
        grpAuthentication.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
        GridLayout glAuthentication = new GridLayout(2, false);
        glAuthentication.horizontalSpacing = 15;
        glAuthentication.verticalSpacing = 10;
        grpAuthentication.setLayout(glAuthentication);
        grpAuthentication.setText("TestCase Mapping");

        createLabel("Mapped TestSuite:");
        txtSuite = createTextbox();
        createLabel("Mapped TestCase:");
        txtCase = createTextbox();
        txtCase.setEnabled(false);
        lblConnectionStatus = new Label(grpAuthentication, SWT.NONE);
        lblConnectionStatus.setText("");
        lblConnectionStatus.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));

        initializeInput();
        return container;
    }

    private void initializeInput() {
        try {
            integrationItem=new QaDebutyTestCaseIntegration();
if(testCaseEntity.getIntegration(Constants.TestCaseIntegrationID)!=null) {
    integrationItem.setProperties(testCaseEntity.getIntegration(Constants.TestCaseIntegrationID).getProperties());
}
            if(integrationItem==null){
                integrationItem=new QaDebutyTestCaseIntegration();

            }
            String data=integrationItem.getSuiteName();
            loadTestSuites();
            if(data!=null&&!data.isEmpty())
            {

                txtSuite.select(txtSuite.indexOf(data));
                txtSuite.setText(data);
                txtCase.setEnabled(true);

                loadTestCases();
                String data2=integrationItem.getTestCaseName();
                if(data2!=null&&!data2.isEmpty()){
                    txtCase.select(txtCase.indexOf(data2));
                    txtCase.setText(data2);
                }else {
                    txtCase.select(txtCase.indexOf("<Not Set>"));
                    txtCase.setText("<Not Set>");
                }

            }else {
                txtSuite.select(txtCase.indexOf("<Not Set>"));
                txtSuite.setText("<Not Set>");
                txtCase.select(txtCase.indexOf("<Not Set>"));
                txtCase.setText("<Not Set>");
            }
            txtSuite.addModifyListener(new ModifyListener() {
                @Override
                public void modifyText(ModifyEvent modifyEvent) {
                    try {



                        if (txtSuite.getText() != null && suitesList != null&&!txtSuite.getText().equals("<Not Set>")) {
                            for (TestSuites suites : suitesList) {
                                if (txtSuite.getText().equals(suites.getName())) {
                                    integrationItem.setSuiteName( txtSuite.getText());
                                    integrationItem.setTestSuiteId(  suites.getTestSuiteId()+"");
                                    edit=true;
                                    txtCase.setEnabled(true);
                                    loadTestCases();

                                    break;
                                }
                            }
                        }else{
                            integrationItem.setSuiteName(null);
                            integrationItem.setTestSuiteId(null);
                            integrationItem.setTestCaseId(null);
                            integrationItem.setTestCaseName(null);
                            edit=true;
                        }

                        partActionService.markDirty();
                    } catch ( Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            txtCase.addModifyListener(new ModifyListener() {
                @Override
                public void modifyText(ModifyEvent modifyEvent) {
                    try {



                        if (txtCase.getText() != null && caseList != null&&!txtCase.getText().equals("<Not Set>")) {
                            for (TestCase caseItem : caseList) {
                                if (txtCase.getText().equals(caseItem.getName())) {
                                    integrationItem.setTestCaseName( txtCase.getText());
                                    integrationItem.setTestCaseId(  caseItem.getTestCaseId()+"");
                                    edit=true;

                                    break;
                                }
                            }
                        }else{
                            integrationItem.setTestCaseId(null);
                            integrationItem.setTestCaseName(null);
                            edit=true;
                        }
                        partActionService.markDirty();
                    } catch ( Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch ( Exception e) {
            e.printStackTrace();
        }
    }

    private void loadTestSuites() {

                final Shell shell = new Shell(parentShell);

                shell.setText("Loading TestSuites...");
                shell.setSize(350, 100);
                ProgressBar progressBar = new ProgressBar(shell, SWT.INDETERMINATE);
                progressBar.setBounds(40, 40, 200, 20);
                shell.open();
                Rectangle parentSize = parentShell.getBounds();
                Rectangle shellSize = shell.getBounds();
                int locationX = (parentSize.width - shellSize.width) / 2 + parentSize.x;
                int locationY = (parentSize.height - shellSize.height) / 2 + parentSize.y;
                shell.setLocation(new Point(locationX, locationY));
                shell.layout();
                txtSuite.removeAll();
                thread = new Thread(() -> {
                    suitesList = WebService.getInstance().getTestSuites();
                    if (suitesList.size() != 0) {
                        syncExec(() -> {
                            txtSuite.add("<Not Set>");
                            for (TestSuites product : suitesList) {
                                txtSuite.add(product.getName());

                            }

                            shell.close();
                        });
                    } else {
                        syncExec(() -> {
                            shell.close();
                            MessageDialog.openWarning(parentShell, "Warning", "There is an issue with the provided credentials or no TestSuites are configured");
                        });
                    }


                });
                thread.start();
            }
    private void loadTestCases() {

        final Shell shell = new Shell(parentShell);

        shell.setText("Loading TestCases...");
        shell.setSize(350, 100);
        ProgressBar progressBar = new ProgressBar(shell, SWT.INDETERMINATE);
        progressBar.setBounds(40, 40, 200, 20);
        shell.open();
        Rectangle parentSize = parentShell.getBounds();
        Rectangle shellSize = shell.getBounds();
        int locationX = (parentSize.width - shellSize.width) / 2 + parentSize.x;
        int locationY = (parentSize.height - shellSize.height) / 2 + parentSize.y;
        shell.setLocation(new Point(locationX, locationY));
        shell.layout();
        txtCase.removeAll();
        thread = new Thread(() -> {
            caseList = WebService.getInstance().getTestCases(integrationItem.getTestSuiteId()+"");
            if (caseList.size() != 0) {
                syncExec(() -> {
                    txtCase.add("<Not Set>");
                    for (TestCase testCase: caseList) {
                        txtCase.add(testCase.getName());

                    }

                    shell.close();
                });
            } else {
                syncExec(() -> {
                    shell.close();
                    MessageDialog.openWarning(parentShell, "Warning", "There is an issue with the provided credentials or no TestCases are configured");
                });
            }


        });
        thread.start();
    }
            void syncExec(Runnable runnable) {
                if (lblConnectionStatus != null && !lblConnectionStatus.isDisposed()) {
                    ApplicationManager.getInstance()
                            .getUIServiceManager()
                            .getService(UISynchronizeService.class)
                            .syncExec(runnable);
                }
            }

            private void createLabel(String labelText) {
                Label label = new Label(grpAuthentication, SWT.NONE);
                label.setText(labelText);
                GridData gridData = new GridData(SWT.LEFT, SWT.TOP, false, false);
                label.setLayoutData(gridData);
            }

            private Combo createTextbox() {
                Combo text = new Combo(grpAuthentication, SWT.BORDER);
                GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
                gridData.widthHint = 200;

                text.setLayoutData(gridData);
                return text;
            }




    @Override
    public Integration getIntegrationBeforeSaving() {
        edit=false;
        return integrationItem;
    }

    @Override
    public boolean needsSaving() {
        return edit;
    }
}
