package com.workwell.plugins.qadeputy.katalon;

import com.katalon.platform.api.preference.PluginPreference;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.Date;

import java.util.ArrayList;
import java.util.List;

public class WebService {
    private static final WebService instance=new WebService();

    private WebService(){

    }

    public static WebService getInstance(){
        return instance;
    }

    List<Products> getProducts(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://app.qadeputy.com/api/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        try{
            PluginPreference pluginStore = CommonUtils.getPluginStore();
            QADeputyAPIClient client=retrofit.create(QADeputyAPIClient.class);
            List<Products> returnValue;
            String userName= pluginStore.getString(Constants.PREF_USERNAME, "");
            String key=pluginStore.getString(Constants.PREF_APIKEY, "");
            Call<List<Products>> callobject= (Call<List<Products>>) client.GetProducts("Bearer "+key,userName,"active","false");
            Response<List<Products>> response=callobject.execute();

            if(response.isSuccessful()){
                returnValue=response.body();

            }else{
                returnValue=new ArrayList<Products>();

            }
            return returnValue;
        }catch (Exception exp){
            return new ArrayList<Products>();

        }
    }

    List<TestSuites> getTestSuites(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://app.qadeputy.com/api/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        try{
            PluginPreference pluginStore = CommonUtils.getPluginStore();
            QADeputyAPIClient client=retrofit.create(QADeputyAPIClient.class);
            List<TestSuites> returnValue=new ArrayList<TestSuites>();
            String userName= pluginStore.getString(Constants.PREF_USERNAME, "");
            String key=pluginStore.getString(Constants.PREF_APIKEY, "");
            int id=pluginStore.getInt(Constants.PREF_PROJECTID, -1);
            if(id==-1){
                return returnValue;
            }
            Call<List<TestSuites>> callobject= (Call<List<TestSuites>>) client.GetTestSuites("Bearer "+key,userName,"active","false");
            Response<List<TestSuites>> response=callobject.execute();

            if(response.isSuccessful()){
                returnValue=response.body();
                returnValue.removeIf(n->n.getProductId()!=id);

            }else{
                returnValue=new ArrayList<TestSuites>();

            }
            return returnValue;
        }catch (Exception exp){
            return new ArrayList<TestSuites>();

        }
    }

    public List<TestCase> getTestCases(String testSuites) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://app.qadeputy.com/api/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        try{
            PluginPreference pluginStore = CommonUtils.getPluginStore();
            QADeputyAPIClient client=retrofit.create(QADeputyAPIClient.class);
            List<TestCase> returnValue=new ArrayList<TestCase>();
            String userName= pluginStore.getString(Constants.PREF_USERNAME, "");
            String key=pluginStore.getString(Constants.PREF_APIKEY, "");
            int id=pluginStore.getInt(Constants.PREF_PROJECTID, -1);
            if(id==-1){
                return returnValue;
            }
            int lastpage=1;
            while(returnValue.size()>=0) {
                Call<ResponseObject> callobject = (Call<ResponseObject>) client.GetTestCase("Bearer " + key, userName, testSuites, "active", "true",50,lastpage);
                Response<ResponseObject> response = callobject.execute();

                if (response.isSuccessful()) {
                    returnValue.addAll(response.body().getTestCaseList());
                    if(response.body().getMetaData().getLastPage()==lastpage){
                        break;
                    }
                } else {
                    returnValue = new ArrayList<TestCase>();
                    break;
                }
                lastpage++;
            }
            return returnValue;
        }catch (Exception exp){
            return new ArrayList<TestCase>();

        }
    }

    public List<TestRuns> getTestRuns(String suiteName) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://app.qadeputy.com/api/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        try{
            PluginPreference pluginStore = CommonUtils.getPluginStore();
            QADeputyAPIClient client=retrofit.create(QADeputyAPIClient.class);
            List<TestRuns> returnValue=new ArrayList<TestRuns>();
            String userName= pluginStore.getString(Constants.PREF_USERNAME, "");
            String key=pluginStore.getString(Constants.PREF_APIKEY, "");
            int id=pluginStore.getInt(Constants.PREF_PROJECTID, -1);
            if(id==-1){
                return returnValue;
            }

            Call<List<TestRuns>> callobject = (Call<List<TestRuns>>) client.GetTestRuns("Bearer " + key, userName, 0, "false");
            Response<List<TestRuns>> response = callobject.execute();

            if (response.isSuccessful()) {
                returnValue.addAll(response.body() );
                returnValue.removeIf(c->!c.getTestSuiteName().equals(suiteName));
            } else {
                returnValue = new ArrayList<TestRuns>();

            }


            return returnValue;
        }catch (Exception exp){
            return new ArrayList<TestRuns>();

        }
    }

    public String   CreateTestRun(String suiteID, String suiteName,List<Integer> testCaseIds) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://app.qadeputy.com/api/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        try{
            PluginPreference pluginStore = CommonUtils.getPluginStore();
            QADeputyAPIClient client=retrofit.create(QADeputyAPIClient.class);
            String returnValue="-1";
            String userName= pluginStore.getString(Constants.PREF_USERNAME, "");
            String key=pluginStore.getString(Constants.PREF_APIKEY, "");
            int projectId=pluginStore.getInt(Constants.PREF_PROJECTID, -1);
            if(projectId==-1){
                return returnValue;
            }

            Call<List<Users>> callobject = (Call<List<Users>>) client.GetTestUsers("Bearer " + key, userName,   "false");
            Response<List<Users>> response = callobject.execute();
            List<Integer> userids=new ArrayList<>();

            if (response.isSuccessful()) {
                List<Users> users=response.body();
                users.forEach(c->{
                   if( checkisadmin(c,userName)){
                       userids.add(c.getUserId() );

                   }
                    for (Product p:c.getProducts()) {
                        if(p.getProductId().equals(projectId)){
                           if(!userids.contains(c.getUserId())){
                               userids.add(c.getUserId() );
                           }

                        }
                    }

                });
                TestRunCreateRequest request=new TestRunCreateRequest();
                request.setDescription("A Katalon Created Test Run");
                Date dt=new Date(System.currentTimeMillis());

                request.setName(suiteName+" "+dt.toString());
                request.setTestCases(testCaseIds);
                request.setIncludeAll(false);
                request.setTestSuite(Integer.parseInt(suiteID));
                request.setUsers(userids);

                Call<TestRuns> createCallObject=client.CreateTestRuns("Bearer "+key,userName,request);
                Response<TestRuns> response2=createCallObject.execute();
                if(response2.isSuccessful()){
                    TestRuns item=response2.body();
                    returnValue=item.getTestRunId()+"";
                }

            }

            return returnValue;
        }catch (Exception exp){
            return "";

        }
    }

    private boolean checkisadmin(Users user,String userName) {
        if(user.getRole()!=null) {
            return user.getRole().toLowerCase().startsWith("admin")||user.getEmail().equals(userName);
        }
        return false;

    }

    public boolean updateTestrun(Integer status, String lastStartedTestRun, String testCaseId) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://app.qadeputy.com/api/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        try{
            PluginPreference pluginStore = CommonUtils.getPluginStore();
            QADeputyAPIClient client=retrofit.create(QADeputyAPIClient.class);
            String returnValue="-1";
            String userName= pluginStore.getString(Constants.PREF_USERNAME, "");
            String key=pluginStore.getString(Constants.PREF_APIKEY, "");
            int projectId=pluginStore.getInt(Constants.PREF_PROJECTID, -1);
            if(projectId==-1){
                return false;
            }

            TestRunStatus request=new TestRunStatus();
            request.setTestCaseStatus(status);
            request.setActualResult("Updated by Katlon integration");
            Call<UpdateTestStatusResult> createCallObject=client.UpdateTestStatus("Bearer "+key,userName,lastStartedTestRun,testCaseId,request);
            Response<UpdateTestStatusResult> response2=createCallObject.execute();
            if(response2.isSuccessful()){
                return true;
            }

        }catch (Exception exp){
            return false;

        }

        return false;

    }
}
