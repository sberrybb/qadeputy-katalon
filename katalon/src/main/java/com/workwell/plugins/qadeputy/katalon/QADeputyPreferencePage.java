package com.workwell.plugins.qadeputy.katalon;

import com.katalon.platform.api.exception.ResourceException;
import com.katalon.platform.api.preference.PluginPreference;
import com.katalon.platform.api.service.ApplicationManager;
import com.katalon.platform.api.ui.UISynchronizeService;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.*;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

import java.util.List;

public class QADeputyPreferencePage extends PreferencePage  {
    private Button chckEnableIntegration;

    private Group grpAuthentication;

    private Text txtUsername;

    private Text txtPassword;



    private Combo txtProject;

    private Composite container;

    private Button btnTestConnection;

    private Label lblConnectionStatus;
    private Shell parentShell;
    private Thread thread;
    private Group grpspot;
    private List<Products> productsList;
    private Products selectedProduct;
    @Override
    protected Control createContents(Composite composite) {
        parentShell=composite.getShell();
        container = new Composite(composite, SWT.NONE);
        container.setLayout(new GridLayout(1, false));

        chckEnableIntegration = new Button(container, SWT.CHECK);
        chckEnableIntegration.setText("Using QADeputy");

        grpAuthentication = new Group(container, SWT.NONE);

        grpAuthentication.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));

        GridLayout glAuthentication = new GridLayout(2, false);
        glAuthentication.horizontalSpacing = 15;
        glAuthentication.verticalSpacing = 10;
        grpAuthentication.setLayout(glAuthentication);
        grpAuthentication.setText("Authentication");



        createLabel("Email");
        txtUsername = createTextbox();

        createLabel("API Key");
        txtPassword = createPasswordTextbox();

        createLabel("Product");
        txtProject = createComboBox();

        btnTestConnection = new Button(grpAuthentication, SWT.PUSH);
        btnTestConnection.setText("Test Connection");
        btnTestConnection.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
              TestConnection();
            }
        });

        lblConnectionStatus = new Label(grpAuthentication, SWT.NONE);
        lblConnectionStatus.setText("");
        lblConnectionStatus.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));

       // handleControlModifyEventListeners();
        initializeInput();

        return container;
    }

    private void TestConnection() {
        performOk();
        final Shell shell=new Shell(parentShell);

        shell.setText("Testing Connection   ...");
        shell.setSize(350, 100);
        ProgressBar progressBar = new ProgressBar(shell, SWT.INDETERMINATE);
        progressBar.setBounds(40, 40, 200, 20);
        shell.open();
        Rectangle parentSize = parentShell.getBounds();
        Rectangle shellSize = shell.getBounds();
        int locationX = (parentSize.width - shellSize.width)/2+parentSize.x;
        int locationY = (parentSize.height - shellSize.height)/2+parentSize.y;
        shell.setLocation(new Point(locationX, locationY));
        shell.layout();
        thread = new Thread(() -> {
            productsList =WebService.getInstance().getProducts();
            if(productsList.size()!=0) {
                syncExec(() -> {
                    MessageDialog.openWarning(getShell(), "Warning", "Connection was successful!");
                    shell.close();
                });
            }else{
                syncExec(() -> {
                    shell.close();
                    MessageDialog.openWarning(getShell(), "Warning", "There is an issue with the provided credentials or no products are configured");
                });
            }

        });
        thread.start();
    }

    private void initializeInput() {
        try {
            PluginPreference pluginStore = CommonUtils.getPluginStore();

            chckEnableIntegration.setSelection(pluginStore.getBoolean(Constants.PREF_ENABLED, false));
            chckEnableIntegration.notifyListeners(SWT.Selection, new Event());

            txtUsername.setText(pluginStore.getString(Constants.PREF_USERNAME, ""));
            txtPassword.setText(pluginStore.getString(Constants.PREF_APIKEY, ""));
            txtPassword.addFocusListener(new FocusListener() {
                @Override
                public void focusGained(FocusEvent focusEvent) {

                }

                @Override
                public void focusLost(FocusEvent focusEvent) {
                    if(!txtPassword.getText().isEmpty()&&
                            !txtUsername.getText().isEmpty())
                   loadProducts();
                }
            });

            txtUsername.addFocusListener(new FocusListener() {
                @Override
                public void focusGained(FocusEvent focusEvent) {

                }

                @Override
                public void focusLost(FocusEvent focusEvent) {
                    if(!txtPassword.getText().isEmpty()&&
                            !txtUsername.getText().isEmpty())
                        loadProducts();
                }
            });


            container.layout(true, true);

            if(  !txtUsername.getText().isEmpty()&&!txtPassword.getText().isEmpty()){
                loadProducts();
            }
            String data=pluginStore.getString( Constants.PREF_PROJECTNAME, "");
           if(!data.isEmpty())
           {

            txtProject.select(txtProject.indexOf(data));
               txtProject.setText(data);
           }
        } catch (ResourceException e) {
            MessageDialog.openWarning(getShell(), "Warning", "Unable to update QaDeputy Integration Settings.");
        }
    }

    private synchronized void loadProducts() {
        performOk();
        final Shell shell=new Shell(parentShell);

        shell.setText("Loading Products...");
        shell.setSize(350, 100);
        ProgressBar progressBar = new ProgressBar(shell, SWT.INDETERMINATE);
        progressBar.setBounds(40, 40, 200, 20);
        shell.open();
        Rectangle parentSize = parentShell.getBounds();
        Rectangle shellSize = shell.getBounds();
        int locationX = (parentSize.width - shellSize.width)/2+parentSize.x;
        int locationY = (parentSize.height - shellSize.height)/2+parentSize.y;
        shell.setLocation(new Point(locationX, locationY));
        shell.layout();
        thread = new Thread(() -> {
         productsList =WebService.getInstance().getProducts();
         if(productsList.size()!=0) {
             syncExec(() -> {
                 for (Products product : productsList) {
                     txtProject.add(product.getName());

                 }
                 shell.close();
             });
         }else{
             syncExec(() -> {
                 shell.close();
                 MessageDialog.openWarning(getShell(), "Warning", "There is an issue with the provided credentials or no products are configured");
             });
         }

        });
          thread.start();
    }
    void syncExec(Runnable runnable) {
        if (lblConnectionStatus != null && !lblConnectionStatus.isDisposed()) {
            ApplicationManager.getInstance()
                    .getUIServiceManager()
                    .getService(UISynchronizeService.class)
                    .syncExec(runnable);
        }
    }
    private Combo createComboBox() {
            Combo text = new Combo(grpAuthentication, SWT.BORDER);
            GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
            gridData.widthHint = 200;

            text.setLayoutData(gridData);
            return text;
    }

    private Text createTextbox() {
        Text text = new Text(grpAuthentication, SWT.BORDER);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        gridData.widthHint = 200;
        text.setLayoutData(gridData);
        return text;
    }

    private Text createPasswordTextbox(){
        Text text = new Text(grpAuthentication, SWT.PASSWORD | SWT.BORDER);
        GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        gridData.widthHint = 200;
        text.setLayoutData(gridData);
        return text;
    }

    private void createLabel(String text) {
        Label label = new Label(grpAuthentication, SWT.NONE);
        label.setText(text);
        GridData gridData = new GridData(SWT.LEFT, SWT.TOP, false, false);
        label.setLayoutData(gridData);
    }
    @Override
    public boolean performOk() {
try {
    PluginPreference pluginStore = CommonUtils.getPluginStore();

    pluginStore.setBoolean(Constants.PREF_ENABLED, chckEnableIntegration.getSelection());
    pluginStore.setString(Constants.PREF_USERNAME, txtUsername.getText());
    pluginStore.setString(Constants.PREF_APIKEY, txtPassword.getText());
if(txtProject.getText()!=null&&productsList!=null) {
    for (Products products : productsList) {
        if (txtProject.getText().equals(products.getName())) {
            pluginStore.setString(Constants.PREF_PROJECTNAME, txtProject.getText());
            pluginStore.setInt(Constants.PREF_PROJECTID, products.getProductId());
            break;
        }
    }
}
    pluginStore.save();
}catch (Exception exp) {
    MessageDialog.openWarning(getShell(), "Warning", "Unable to update QaDeputy Integration Settings.");

    return false;

}
        return true;
   }
}
