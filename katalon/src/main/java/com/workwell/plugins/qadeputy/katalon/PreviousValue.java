package com.workwell.plugins.qadeputy.katalon;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PreviousValue {

    @SerializedName("test_case_status")
    @Expose
    private Integer testCaseStatus;
    @SerializedName("actual_result")
    @Expose
    private Object actualResult;

    public Integer getTestCaseStatus() {
        return testCaseStatus;
    }

    public void setTestCaseStatus(Integer testCaseStatus) {
        this.testCaseStatus = testCaseStatus;
    }

    public Object getActualResult() {
        return actualResult;
    }

    public void setActualResult(Object actualResult) {
        this.actualResult = actualResult;
    }

}