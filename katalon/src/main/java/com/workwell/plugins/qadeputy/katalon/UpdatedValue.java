package com.workwell.plugins.qadeputy.katalon;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdatedValue {

    @SerializedName("test_case_status")
    @Expose
    private Integer testCaseStatus;
    @SerializedName("actual_result")
    @Expose
    private String actualResult;

    public Integer getTestCaseStatus() {
        return testCaseStatus;
    }

    public void setTestCaseStatus(Integer testCaseStatus) {
        this.testCaseStatus = testCaseStatus;
    }

    public String getActualResult() {
        return actualResult;
    }

    public void setActualResult(String actualResult) {
        this.actualResult = actualResult;
    }

}
