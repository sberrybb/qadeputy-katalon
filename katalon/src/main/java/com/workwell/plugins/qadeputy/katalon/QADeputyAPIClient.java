package com.workwell.plugins.qadeputy.katalon;

import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

public interface QADeputyAPIClient {

   @GET("products")
   @Headers({
           "Accept: application/json",
           "User-Agent: KatlonPlugin",
           "Content-Type: application/json"
   })
   Call<List<Products>> GetProducts(@Header("Authorization") String apiKey, @Header("Email") String email, @Query("product_status")String productStatus, @Query("pagination") String usePaging);
   @GET("test-suites")
   @Headers({
           "Accept: application/json",
           "User-Agent: KatlonPlugin",
           "Content-Type: application/json"
   })
   Call<List<TestSuites>> GetTestSuites(@Header("Authorization") String apiKey, @Header("Email") String email, @Query("test_suite_status")String testSuiteStatus, @Query("pagination") String usePaging);

   @GET("test-suites/{test_suite_id_1}/test-cases")
   @Headers({
           "Accept: application/json",
           "User-Agent: KatlonPlugin",
           "Content-Type: application/json"
   })
   Call<ResponseObject> GetTestCase(@Header("Authorization") String apiKey, @Header("Email") String email,@Path("test_suite_id_1")String suiteId, @Query("test_case_status")String testSuiteStatus, @Query("pagination") String usePaging, @Query("per_page")int perPage,@Query("page")int page);

   @GET("test-suites/{test_suite_id_1}/test-cases")
   @Headers({
           "Accept: application/json",
           "User-Agent: KatlonPlugin",
           "Content-Type: application/json"
   })
   Call<List<TestRuns>> GetTestRuns(@Header("Authorization") String apiKey, @Header("Email") String email,  @Query("is_completed")int isCompleted, @Query("pagination") String usePaging);

   @GET("users")
   @Headers({
           "Accept: application/json",
           "User-Agent: KatlonPlugin",
           "Content-Type: application/json"
   })
   Call<List<Users>> GetTestUsers(@Header("Authorization") String apiKey, @Header("Email") String email, @Query("pagination") String usePaging);

   @POST("test-runs")
   @Headers({
           "Accept: application/json",
           "User-Agent: KatlonPlugin",
           "Content-Type: application/json"
   })
   Call<TestRuns> CreateTestRuns(@Header("Authorization") String apiKey, @Header("Email") String email, @Body  TestRunCreateRequest request);

   @PUT("test-runs/{test_run_id}/test-cases/{test_case_id}")
   @Headers({
           "Accept: application/json",
           "User-Agent: KatlonPlugin",
           "Content-Type: application/json"
   })
   Call<UpdateTestStatusResult> UpdateTestStatus(@Header("Authorization") String apiKey, @Header("Email") String email, @Path("test_run_id")String testRunid,@Path("test_case_id")String testCaseId,@Body TestRunStatus status );

}
